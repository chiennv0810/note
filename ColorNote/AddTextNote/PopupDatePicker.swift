//
//  PopupDatePicker.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 1/13/21.
//

import UIKit

class PopupDatePicker: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var viewBack: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapBack(_:))))
        
        viewPopup.layer.masksToBounds = true
        viewPopup.layer.cornerRadius = scale*30
        
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
        
    }
    
    @objc func tapBack(_ sender: UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    

    @IBAction func datePicker(_ sender: Any) {
        let strDate = formaterDate(datePicker.date)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sendDatePicker"), object: nil, userInfo: ["text": strDate ])
    }
}
