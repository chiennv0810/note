//
//  ContentTextNoteVC.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/24/20.
//

import UIKit
class ContentTextNoteVC: UIViewController, UITextViewDelegate {
    @IBOutlet weak var contentText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        contentText.delegate = self
        let status = UserDefaults.standard.string(forKey: "nextSc")
        if status! == "nextSc" {
            contentText.text = TextNoteEntity.shared.getTextNote(idCheck: AddNoteVC.shared.id)
            let color = TextNoteEntity.shared.getDataColor(idCheck: AddNoteVC.shared.id)
            if color == ColorDefine.colorWhite {
                contentText.textColor = UIColor(hexString: "#000000")
            } else {
                contentText.textColor = UIColor(hexString: "#ffffff")
            }
        }
        
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "contentTextNote"), object: nil, userInfo: ["text":textView.text ?? ""])
    }
    
    
    
}
