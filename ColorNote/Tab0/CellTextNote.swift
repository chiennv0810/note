//
//  CellTextNote.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 12/29/20.
//

import UIKit

class CellTextNote: UICollectionViewCell {

    @IBOutlet weak var textNoteBottom: UILabel!
    @IBOutlet weak var borderTextNoteBottom: UIView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var content: UITextView!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBg.layer.cornerRadius = scale * 16
        borderTextNoteBottom.layer.cornerRadius = scale * 5
        borderTextNoteBottom.layer.borderWidth = scale * 1
        
    }

}
