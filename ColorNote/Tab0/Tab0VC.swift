//
//  Tab0VC.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/22/20.
//

import UIKit
import collection_view_layouts

class Tab0VC: UIViewController{
    var indexSelect = -1
    
    var listDataTextNote = [ModelTextNote]()
    var listDataCheckList = [ModelCheckList]()
    var layoutPinterest: PinterestStyleFlowLayout = PinterestStyleFlowLayout()
    var viewType:String = "largeGrid"
    var filterData = [ModelTextNote]()
    @IBOutlet weak var clvData: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Sqldata.shared.loadInit()
        listDataTextNote = TextNoteEntity.shared.getData()
        filterData = listDataTextNote
        
        
        
        clvData.dataSource = self
        clvData.delegate = self
        clvData.backgroundColor = .clear
        
        layoutPinterest.delegate = self
        
        //Register Cell
        clvData.register(UINib(nibName: "CellTextNote", bundle: nil), forCellWithReuseIdentifier: "CellTextNote")
        clvData.register(UINib(nibName: "CellCheckList", bundle: nil), forCellWithReuseIdentifier: "CellCheckList")
        clvData.register(UINib(nibName: "CellList", bundle: nil), forCellWithReuseIdentifier: "CellList")
        clvData.register(UINib(nibName: "CellDetail", bundle: nil), forCellWithReuseIdentifier: "CellDetail")
        clvData.register(UINib(nibName: "CellGrid", bundle: nil), forCellWithReuseIdentifier: "CellGrid")
        
        let getViewType = UserDefaults.standard.string(forKey: "sortByView") ?? ""
        if getViewType == "" {
            layoutPinterest.cellsPadding = ItemsPadding(horizontal: scale * 10, vertical: scale * 10)
            clvData.collectionViewLayout = layoutPinterest
        } else {
            viewType = getViewType
            if viewType == "largeGrid"{
                layoutPinterest.cellsPadding = ItemsPadding(horizontal: scale * 10, vertical: scale * 10)
                clvData.collectionViewLayout = layoutPinterest
            } else {
                clvData.collectionViewLayout = UICollectionViewFlowLayout()
            }
        }
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reload(_:)), name: NSNotification.Name(rawValue: "reloadData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSortByView(_:)), name: NSNotification.Name(rawValue: "reloadSortByView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadFilterByColor(_:)), name: NSNotification.Name(rawValue: "reloadFilterByColor"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSortByAlphabet(_:)), name: NSNotification.Name(rawValue: "reloadSortByAlphabet"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSortByColor(_:)), name: NSNotification.Name(rawValue: "reloadSortByColor"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSortByCreateTime(_:)), name: NSNotification.Name(rawValue: "reloadSortByCreateTime"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSortByModifyTime(_:)), name: NSNotification.Name(rawValue: "reloadSortByModifyTime"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSortByReminder(_:)), name: NSNotification.Name(rawValue: "reloadSortByReminder"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.listenedTfSearch(_:)), name: NSNotification.Name(rawValue: "textSearch"), object: nil)
        
    }
    
    @objc func listenedTfSearch(_ sender: Notification){
        let tf = sender.userInfo?["text"] as! String
        print("@@@")
        print(tf)
        filterData = []
            if tf == "" {
                filterData = self.listDataTextNote
            } else {
                for item in listDataTextNote {
                    if item.title.lowercased().contains((tf.lowercased())){
                        filterData.append(item)
                    }
                }
            }
        self.clvData.reloadData()
    }
    
    
    @IBAction func btnSortBy(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PopupSortBy") as! PopupSortBy
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    @objc func reload(_ sender: Notification){
        filterData = TextNoteEntity.shared.getData()
        clvData.reloadData()
    }
    
    @objc func reloadSortByView(_ sender: Notification){
        viewType = UserDefaults.standard.string(forKey: "sortByView") ?? ""
        if viewType == "largeGrid"{
            layoutPinterest.cellsPadding = ItemsPadding(horizontal: scale * 10, vertical: scale * 10)
            clvData.collectionViewLayout = layoutPinterest
        } else {
            clvData.collectionViewLayout = UICollectionViewFlowLayout()
        }
        clvData.reloadData()
    }
    
    @objc func reloadFilterByColor(_ sender: Notification){
        let getSortColor = UserDefaults.standard.string(forKey: "keyColor") ?? ""
        if getSortColor != "" {
            filterData = TextNoteEntity.shared.getDataWhereColor(colorFilter: getSortColor)
        } else {
            filterData = TextNoteEntity.shared.getData()
        }
        
        clvData.reloadData()
    }
    
    @objc func reloadSortByAlphabet(_ sender: Notification){
        filterData = TextNoteEntity.shared.sortByAlphabet()
        clvData.reloadData()
    }
    
    @objc func reloadSortByColor(_ sender: Notification){
        filterData = TextNoteEntity.shared.sortByColor()
        clvData.reloadData()
    }
    
    @objc func reloadSortByCreateTime(_ sender: Notification){
        filterData = TextNoteEntity.shared.sortByCreateTime()
        clvData.reloadData()
    }
    
    @objc func reloadSortByModifyTime(_ sender: Notification){
        filterData = TextNoteEntity.shared.sortByModifyTime()
        clvData.reloadData()
    }
    
    @objc func reloadSortByReminder(_ sender: Notification){
        filterData = TextNoteEntity.shared.sortByReminderTime()
        clvData.reloadData()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}



extension Tab0VC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ContentDynamicLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if viewType == "list" {
            return CGSize(width: scale * 365, height: scale * 69)
        } else if viewType == "detail" {
            return CGSize(width: scale * 365, height: scale * 168)
        } else {
            return CGSize(width: scale * 115, height: scale * 115)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: scale * 75, right: 0)
    }
    
    func cellSize(indexPath: IndexPath) -> CGSize {
        if indexPath.row == listDataTextNote.count-1{
            layoutPinterest.contentPadding = ItemsPadding(horizontal: 0, vertical:  scale * 75)
        }
        if filterData[indexPath.row].type == 1 {
            return CGSize(width: scale*182, height: scale*300)
        } else {
            let id = filterData[indexPath.row].id
            listDataCheckList = CheckListEntity.shared.getData(idCheck: id)
            return CGSize(width: scale*182, height: scale * 25 * CGFloat(listDataCheckList.count) + scale * 130)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print(filterData[indexPath.row].reminder)
        
        switch viewType {
        case "list":
            let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellList", for: indexPath) as! CellList
            cell.viewBackground.backgroundColor = UIColor(hexString: filterData[indexPath.row].color).withAlphaComponent(0.33)
            cell.viewColorLeft.backgroundColor = UIColor(hexString: filterData[indexPath.row].color)
            cell.lblTitle.text = filterData[indexPath.row].title
            if filterData[indexPath.row].modifyTime == "" {
                cell.lblDate.text = filterData[indexPath.row].createTime
            } else {
                cell.lblDate.text = filterData[indexPath.row].modifyTime
            }
            return cell
        case "detail":
            let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellDetail", for: indexPath) as! CellDetail
            cell.viewBackground.backgroundColor = UIColor(hexString: filterData[indexPath.row].color).withAlphaComponent(0.33)
            cell.viewColorLeft.backgroundColor = UIColor(hexString: filterData[indexPath.row].color)
            cell.lblTitle.text = filterData[indexPath.row].title
            if filterData[indexPath.row].textNote != "" {
                cell.lblText.text = filterData[indexPath.row].textNote
            } else {
                cell.lblText.text = "Click me to show Check List"
            }
            
            if filterData[indexPath.row].modifyTime == "" {
                cell.lblDate.text = filterData[indexPath.row].createTime
            } else {
                cell.lblDate.text = filterData[indexPath.row].modifyTime
            }
            
            return cell
        case "grid":
            let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellGrid", for: indexPath) as! CellGrid
            cell.viewColorTop.backgroundColor = UIColor(hexString: filterData[indexPath.row].color).withAlphaComponent(0.33)
            cell.viewBackground.backgroundColor = .black
            let type = filterData[indexPath.row].type
            if type == 1 {
                cell.lblType.text = "Text Note"
            } else {
                cell.lblType.text = "Checklist"
            }
            cell.viewColorLeft.backgroundColor = UIColor(hexString: filterData[indexPath.row].color)
            cell.lblTitle.text = filterData[indexPath.row].title
            if filterData[indexPath.row].modifyTime == "" {
                cell.lblDate.text = filterData[indexPath.row].createTime
            } else {
                cell.lblDate.text = filterData[indexPath.row].modifyTime
            }
            return cell
        case "largeGrid":
            if filterData[indexPath.row].type == 1 {
                let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellTextNote", for: indexPath) as! CellTextNote
                cell.viewBg.backgroundColor = UIColor(hexString: filterData[indexPath.row].color)
                cell.title.text = filterData[indexPath.row].title
                if filterData[indexPath.row].modifyTime == "" {
                    cell.date.text = filterData[indexPath.row].createTime
                } else {
                    cell.date.text = filterData[indexPath.row].modifyTime
                }
                cell.content.text = filterData[indexPath.row].textNote
                
                if cell.viewBg.backgroundColor == UIColor(hexString: "#ffffff"){
                    cell.title.textColor = UIColor.black
                    cell.date.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                    cell.content.textColor = UIColor.black
                    cell.borderTextNoteBottom.layer.borderColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6).cgColor
                    cell.textNoteBottom.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                    cell.date.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                } else {
                    cell.title.textColor = UIColor.white
                    cell.content.textColor = UIColor.white
                    cell.borderTextNoteBottom.layer.borderColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6).cgColor
                    cell.textNoteBottom.textColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6)
                    cell.date.textColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6)
                }
                
                return cell
            } else {
                let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellCheckList", for: indexPath) as! CellCheckList
                cell.setup(id: filterData[indexPath.row].id)
                cell.viewBg.backgroundColor = UIColor(hexString: filterData[indexPath.row].color)
                cell.title.text = filterData[indexPath.row].title
                if filterData[indexPath.row].modifyTime == "" {
                    cell.date.text = filterData[indexPath.row].createTime
                } else {
                    cell.date.text = filterData[indexPath.row].modifyTime
                }
                if cell.viewBg.backgroundColor == UIColor(hexString: "#ffffff"){
                    cell.title.textColor = UIColor.black
                    cell.date.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                    cell.date.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                    cell.checklistBottom.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                } else {
                    cell.title.textColor = UIColor.white
                    cell.date.textColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6)
                    cell.checklistBottom.textColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6)
                }
                
                return cell
            }
        default:
            if filterData[indexPath.row].type == 1 {
                let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellTextNote", for: indexPath) as! CellTextNote
                cell.viewBg.backgroundColor = UIColor(hexString: filterData[indexPath.row].color)
                cell.title.text = filterData[indexPath.row].title
                if filterData[indexPath.row].modifyTime == "" {
                    cell.date.text = filterData[indexPath.row].createTime
                } else {
                    cell.date.text = filterData[indexPath.row].modifyTime
                }
                cell.content.text = filterData[indexPath.row].textNote
                
                if cell.viewBg.backgroundColor == UIColor(hexString: "#ffffff"){
                    cell.title.textColor = UIColor.black
                    cell.date.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                    cell.content.textColor = UIColor.black
                    cell.borderTextNoteBottom.layer.borderColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6).cgColor
                    cell.textNoteBottom.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                    cell.date.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                } else {
                    cell.title.textColor = UIColor.white
                    cell.content.textColor = UIColor.white
                    cell.borderTextNoteBottom.layer.borderColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6).cgColor
                    cell.textNoteBottom.textColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6)
                    cell.date.textColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6)
                }
                
                return cell
            } else {
                let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellCheckList", for: indexPath) as! CellCheckList
                cell.setup(id: filterData[indexPath.row].id)
                cell.viewBg.backgroundColor = UIColor(hexString: filterData[indexPath.row].color)
                cell.title.text = filterData[indexPath.row].title
                if filterData[indexPath.row].modifyTime == "" {
                    cell.date.text = filterData[indexPath.row].createTime
                } else {
                    cell.date.text = filterData[indexPath.row].modifyTime
                }
                if cell.viewBg.backgroundColor == UIColor(hexString: "#ffffff"){
                    cell.title.textColor = UIColor.black
                    cell.date.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                    cell.date.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                    cell.checklistBottom.textColor = UIColor(rgb: 0x000000).withAlphaComponent(0.6)
                } else {
                    cell.title.textColor = UIColor.white
                    cell.date.textColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6)
                    cell.checklistBottom.textColor = UIColor(rgb: 0xFFFFFF).withAlphaComponent(0.6)
                }
                
                return cell
                
                
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indexSelect = indexPath.row
        if indexSelect == indexPath.row {
            let type = filterData[indexPath.row].type
            if type == 1 {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddNoteVC") as! AddNoteVC
                vc.titleScreen = "Text notes"
                vc.type = 1
                vc.id = filterData[indexPath.row].id
                AddNoteVC.shared.id = vc.id
                vc.status = "nextSc"
                UserDefaults.standard.set("nextSc", forKey: "nextSc")
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddNoteVC") as! AddNoteVC
                vc.titleScreen = "Checklist"
                vc.type = 2
                vc.id = filterData[indexPath.row].id
                AddNoteVC.shared.id = vc.id
                vc.status = "nextSc"
                UserDefaults.standard.set("nextSc", forKey: "nextSc")
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        clvData.reloadData()
    }
    
}

