//
//  PopupSortBy.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/25/20.
//

import UIKit

class PopupSortBy: UIViewController {

    @IBOutlet weak var olBtnView: UIButton!
    @IBOutlet weak var olBtnSortBy: UIButton!
    @IBOutlet weak var olBtnColor: UIButton!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lineSortBy: UIView!
    @IBOutlet weak var lineColor: UIView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewSortBy: UIView!
    @IBOutlet weak var viewColor: UIView!
    @IBOutlet weak var viewView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapBack(_:))))
        
        viewPopup.layer.masksToBounds = true
        viewPopup.layer.cornerRadius = scale*30
        
    }
    
    @objc func tapBack(_ sender: UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewSortBy.isHidden = true
        viewColor.isHidden = false
        viewView.isHidden = true
        
        lineColor.isHidden = false
        lineSortBy.isHidden = true
        lineView.isHidden = true
        
        olBtnColor.setImage(UIImage(named: "sortByColorSelected"), for: .normal)
        olBtnSortBy.setImage(UIImage(named: "sortBy"), for: .normal)
        olBtnView.setImage(UIImage(named: "sortByView"), for: .normal)
    }
    
    @IBAction func btnColor(_ sender: Any) {
        viewSortBy.isHidden = true
        viewColor.isHidden = false
        viewView.isHidden = true
        
        lineColor.isHidden = false
        lineSortBy.isHidden = true
        lineView.isHidden = true
        
        olBtnColor.setImage(UIImage(named: "sortByColorSelected"), for: .normal)
        olBtnSortBy.setImage(UIImage(named: "sortBy"), for: .normal)
        olBtnView.setImage(UIImage(named: "sortByView"), for: .normal)
    }
    
    @IBAction func btnSortBy(_ sender: Any) {
        viewSortBy.isHidden = false
        viewColor.isHidden = true
        viewView.isHidden = true
        
        lineColor.isHidden = true
        lineSortBy.isHidden = false
        lineView.isHidden = true
        
        olBtnColor.setImage(UIImage(named: "sortByColor"), for: .normal)
        olBtnSortBy.setImage(UIImage(named: "sortBySelected"), for: .normal)
        olBtnView.setImage(UIImage(named: "sortByView"), for: .normal)
    }
    
    @IBAction func btnView(_ sender: Any) {
        viewSortBy.isHidden = true
        viewColor.isHidden = true
        viewView.isHidden = false
        
        lineColor.isHidden = true
        lineSortBy.isHidden = true
        lineView.isHidden = false
        
        olBtnColor.setImage(UIImage(named: "sortByColor"), for: .normal)
        olBtnSortBy.setImage(UIImage(named: "sortBy"), for: .normal)
        olBtnView.setImage(UIImage(named: "sortByViewSelected"), for: .normal)
    }
  

}
