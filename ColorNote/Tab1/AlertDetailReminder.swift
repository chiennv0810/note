//
//  AlertDetailReminder.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 1/19/21.
//

import UIKit

class AlertDetailReminder: UIViewController {
    var date: String = "" {
        didSet {
            listData = TextNoteEntity.shared.getDataReminder(rmd: date)
        }
    }
    var listData = [ModelTextNote]()

    @IBOutlet weak var clvData: UICollectionView!
    @IBOutlet weak var lblDate: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clvData.dataSource = self
        clvData.delegate = self
        clvData.backgroundColor = .clear
        clvData.register(UINib(nibName: "CellList", bundle: nil), forCellWithReuseIdentifier: "CellList")
        print("sssssssśsssss \(date) ")
        listData = TextNoteEntity.shared.getDataReminder(rmd: date)
        print(listData.count)
    }
    @IBAction func btnPrevious(_ sender: Any) {
    }
    @IBAction func btnNext(_ sender: Any) {
    }
    @IBAction func btnAddNewNote(_ sender: Any) {
    }
    
}

extension AlertDetailReminder: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        listData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellList", for: indexPath) as! CellList
        cell.viewBackground.backgroundColor = UIColor(hexString: listData[indexPath.row].color).withAlphaComponent(0.33)
        cell.viewColorLeft.backgroundColor = UIColor(hexString: listData[indexPath.row].color)
        cell.lblTitle.text = listData[indexPath.row].title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: scale * 365, height: scale * 69)
    }
}
