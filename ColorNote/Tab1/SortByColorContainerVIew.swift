//
//  SortByColorContainerVIew.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/25/20.
//

import UIKit

class SortByColorContainerVIew: UIViewController {

    @IBOutlet weak var colorWhite: UIView!
    @IBOutlet weak var colorYellow: UIView!
    @IBOutlet weak var colorGreen: UIView!
    @IBOutlet weak var colorPurple: UIView!
    @IBOutlet weak var colorGrey: UIView!
    @IBOutlet weak var colorRed: UIView!
    @IBOutlet weak var colorOrange: UIView!
    @IBOutlet weak var colorBlue: UIView!
    @IBOutlet weak var colorMoss: UIView!
    @IBOutlet weak var colorBlack: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        colorBlack.layer.cornerRadius = scale*30
        if #available(iOS 11.0, *) {
            colorBlack.layer.maskedCorners = [.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        colorGrey.layer.cornerRadius = scale*30
        if #available(iOS 11.0, *) {
            colorGrey.layer.maskedCorners = [.layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        colorWhite.backgroundColor = UIColor(hexString: ColorDefine.colorWhite)
        colorYellow.backgroundColor = UIColor(hexString: ColorDefine.colorYellow)
        colorGreen.backgroundColor = UIColor(hexString: ColorDefine.colorGreen)
        colorPurple.backgroundColor = UIColor(hexString: ColorDefine.colorPurple)
        colorGrey.backgroundColor = UIColor(hexString: ColorDefine.colorGrey)
        colorRed.backgroundColor = UIColor(hexString: ColorDefine.colorRed)
        colorOrange.backgroundColor = UIColor(hexString: ColorDefine.colorOrange)
        colorBlue.backgroundColor = UIColor(hexString: ColorDefine.colorBlue)
        colorMoss.backgroundColor = UIColor(hexString: ColorDefine.colorMoss)
        colorBlack.backgroundColor = UIColor(hexString: ColorDefine.colorBlack)
 
        
        colorWhite.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorWhite(_:))))
        colorYellow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorYellow(_:))))
        colorGreen.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorGreen(_:))))
        colorPurple.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorPurple(_:))))
        colorGrey.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorGrey(_:))))
        colorRed.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorRed(_:))))
        colorOrange.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorOrange(_:))))
        colorBlue.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorBlue(_:))))
        colorMoss.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorMoss(_:))))
        colorBlack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(colorBlack(_:))))
        
        
    }
    
    func pushNotification(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadFilterByColor"), object: nil)
    }
    
    @objc func colorWhite(_ sender: UITapGestureRecognizer) {

            UserDefaults.standard.set(ColorDefine.colorWhite, forKey: "keyColor")
        pushNotification()
    }
    @objc func colorYellow(_ sender: UITapGestureRecognizer) {
            UserDefaults.standard.set(ColorDefine.colorYellow, forKey: "keyColor")
        
        pushNotification()
    }
    @objc func colorGreen(_ sender: UITapGestureRecognizer) {
            UserDefaults.standard.set(ColorDefine.colorGreen, forKey: "keyColor")
       
        pushNotification()
    }
    @objc func colorPurple(_ sender: UITapGestureRecognizer) {
            UserDefaults.standard.set(ColorDefine.colorPurple, forKey: "keyColor")
      
        pushNotification()
    }
    @objc func colorGrey(_ sender: UITapGestureRecognizer) {
            UserDefaults.standard.set(ColorDefine.colorGrey, forKey: "keyColor")
       
        pushNotification()
    }
    @objc func colorRed(_ sender: UITapGestureRecognizer) {
            UserDefaults.standard.set(ColorDefine.colorRed, forKey: "keyColor")
    
        pushNotification()
    }
    @objc func colorOrange(_ sender: UITapGestureRecognizer) {

            UserDefaults.standard.set(ColorDefine.colorOrange, forKey: "keyColor")
     
        pushNotification()
    }
    @objc func colorBlue(_ sender: UITapGestureRecognizer) {
            UserDefaults.standard.set(ColorDefine.colorBlue, forKey: "keyColor")
     
        pushNotification()
    }
    @objc func colorMoss(_ sender: UITapGestureRecognizer) {
            UserDefaults.standard.set(ColorDefine.colorMoss, forKey: "keyColor")
    
        pushNotification()
    }
    @objc func colorBlack(_ sender: UITapGestureRecognizer) {
            UserDefaults.standard.set(ColorDefine.colorBlack, forKey: "keyColor")
    
        pushNotification()
    }

}
