//
//  Tab1VC.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/22/20.
//

import UIKit
import FSCalendar

class Tab1VC: UIViewController {
    var listData = [ModelTextNote]()
    let fomarter = DateFormatter()
    @IBOutlet weak var calendar: FSCalendar!
    override func viewDidLoad() {
        super.viewDidLoad()
        calendar.delegate = self
        calendar.dataSource = self
        listData = TextNoteEntity.shared.getData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reload(_:)), name: NSNotification.Name(rawValue: "reloadData"), object: nil)
    }
    @objc func reload(_ sender: Notification){
        listData = TextNoteEntity.shared.getData()
        calendar.reloadData()
    }
    
}

extension Tab1VC: FSCalendarDelegate, FSCalendarDataSource{
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        fomarter.dateFormat = "dd-MM-yyyy"
        let listDataReminder = TextNoteEntity.shared.getDataReminder(rmd: fomarter.string(from: date))
        if listDataReminder.count != 0 {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            let customeAlert = AlertDetailReminder()
            customeAlert.date = fomarter.string(from: date)
            customeAlert.preferredContentSize = customeAlert.view.bounds.size
            alertController.setValue(customeAlert, forKey: "contentViewController")
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        fomarter.dateFormat = "dd-MM-yyyy"
        for i in listData {
            guard let evendate = fomarter.date(from: i.reminder) else {return 0}
            if date.compare(evendate) == .orderedSame {
                return 1
            }
        }
        return 0
    }
}

