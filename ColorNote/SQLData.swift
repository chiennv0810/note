//
//  SQLData.swift
//  ColorNote
//
//  Created by Nguyễn Văn Chiến on 12/29/20.
//

import Foundation
import SQLite

class Sqldata{
    
    // tạo 1 singleton để gọi sqldatabáse
    static let shared = Sqldata()
    
    // khai báo phương thức connection
    public var connection: Connection?
    func loadInit(){
        let dbURL = Bundle.main.url(forResource: "dbColorNote", withExtension: "db")!
        
        var newURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        newURL.appendPathComponent("dbColorNote")
        do {
            if FileManager.default.fileExists(atPath: newURL.path) {
//                try FileManager.default.removeItem(atPath: newURL.path)
                print("sss")
            }
            try FileManager.default.copyItem(atPath: dbURL.path, toPath: newURL.path)
            print(newURL.path)
        } catch {
            print(error.localizedDescription)
        }
        
        do{
            connection = try Connection(newURL.path)
        } catch{
            connection = nil
            let nserr = error as NSError
            print("Cannot connect to Database. Error is: \(nserr), \(nserr.userInfo)")
        }
    }
    
    
}
