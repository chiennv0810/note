//
//  Tab3VC.swift
//  ColorNote
//
//  Created by Apple on 1/12/21.
//

import UIKit
import Toast_Swift

class Tab3VC: UIViewController {
    var listDataTextNote = [ModelTextNote]()
    var indexSelect = -1

    @IBOutlet weak var clvData: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataDelete(_:)), name: NSNotification.Name(rawValue: "reloadDataDelete"), object: nil)
        
        listDataTextNote = TextNoteEntity.shared.getDataDelete()
        clvData.dataSource = self
        clvData.delegate = self
        clvData.backgroundColor = .clear
        clvData.register(UINib(nibName: "CellList", bundle: nil), forCellWithReuseIdentifier: "CellList")
    }
    
    @objc func reloadDataDelete(_ sender: Notification){
        listDataTextNote = TextNoteEntity.shared.getDataDelete()
        clvData.reloadData()
    }

}
extension Tab3VC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listDataTextNote.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellList", for: indexPath) as! CellList
        cell.viewBackground.backgroundColor = UIColor(hexString: listDataTextNote[indexPath.row].color).withAlphaComponent(0.33)
        cell.viewColorLeft.backgroundColor = UIColor(hexString: listDataTextNote[indexPath.row].color)
        cell.lblTitle.text = listDataTextNote[indexPath.row].title
        if listDataTextNote[indexPath.row].modifyTime == "" {
            cell.lblDate.text = listDataTextNote[indexPath.row].createTime
        } else {
            cell.lblDate.text = listDataTextNote[indexPath.row].modifyTime
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: scale * 365, height: scale * 69)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: scale * 75, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indexSelect = indexPath.row

        if indexSelect == indexPath.row {
            let type = TextNoteEntity.shared.getType(idCheck: self.listDataTextNote[indexPath.row].id)
            var nameType = ""
            if type == 1 {
                nameType = "Text note"
            } else {
                nameType = "Checklist"
            }
            let alert = UIAlertController(title: "Delete this " + nameType, message: "After deleting, this " + nameType + " will disappear in memory", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
                TextNoteEntity.shared.deleteRow(idDeltete: self.listDataTextNote[indexPath.row].id)
                self.listDataTextNote = TextNoteEntity.shared.getDataDelete()
                self.clvData.reloadData()
                self.navigationController?.view.makeToast("This " + nameType + " deleted")
                
            }))
            alert.addAction(UIAlertAction(title: "Restore", style: .default, handler: { action in
                TextNoteEntity.shared.updateDelete(idCheck: self.listDataTextNote[indexPath.row].id, deleteUpdate: TextNoteEntity.shared.getStatusDelete(idCheck: self.listDataTextNote[indexPath.row].id))
                self.listDataTextNote = TextNoteEntity.shared.getDataDelete()
                self.clvData.reloadData()
                self.navigationController?.view.makeToast("This " + nameType + " restored")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadData"), object: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                print("Cancel")
            }))
            self.present(alert, animated: true)
        }
        
    }
}
