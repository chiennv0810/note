//
//  SettingVC.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/24/20.
//

import UIKit

class SettingVC: UIViewController {

    @IBOutlet weak var autoBackUp: UIView!
    @IBOutlet weak var defaultOrderInColor: UIView!
    @IBOutlet weak var defauleNoteSortOrder: UIView!
    @IBOutlet weak var listItemHeight: UIView!
    @IBOutlet weak var defaultFontSize: UIView!
    @IBOutlet weak var defaultColor: UIView!
    @IBOutlet weak var defaultScreen: UIView!
    @IBOutlet weak var syncOnLaunch: UIView!
    
    
    @IBOutlet weak var circleAutoBackup: UIView!
    @IBOutlet weak var viewDefaultColor: UIView!
    @IBOutlet weak var circleSyncOnLaunch: UIView!
    @IBOutlet weak var viewTop: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Radius viewTop
        viewTop.clipsToBounds = true
        viewTop.layer.cornerRadius = scale*30
        if #available(iOS 11.0, *) {
            viewTop.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        //Circle view SyncOnLaunch
        circleSyncOnLaunch.layer.cornerRadius = scale*8
        circleSyncOnLaunch.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        circleSyncOnLaunch.layer.borderWidth = scale*1
        
        //Circle view Default Color
        let color = UserDefaults.standard.string(forKey: "colorDefault") ?? ""
        if color == "" {
            UserDefaults.standard.set(ColorDefine.colorWhite, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorWhite)
        } else {
            self.viewDefaultColor.backgroundColor = UIColor(hexString: color)
        }
        viewDefaultColor.layer.cornerRadius = scale*15
        
        //Circle autoBackup
        circleAutoBackup.layer.cornerRadius = scale*8
        circleAutoBackup.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        circleAutoBackup.layer.borderWidth = scale*1
        
        autoBackUp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(autoBackUp(_:))))
        defaultOrderInColor.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(defaultOrderInColor(_:))))
        defauleNoteSortOrder.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(defauleNoteSortOrder(_:))))
        syncOnLaunch.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(syncOnLaunch(_:))))
        listItemHeight.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(listItemHeight(_:))))
        defaultFontSize.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(defaultFontSize(_:))))
        defaultColor.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(defaultColor(_:))))
        defaultScreen.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(defaultScreen(_:))))
        
    }
    
    @objc func autoBackUp(_ sender: UITapGestureRecognizer){
        
    }
    @objc func defaultOrderInColor(_ sender: UITapGestureRecognizer){
        let alert = UIAlertController(title: "Default order in color", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "By modified time", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "By create time", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "By alphabet", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "By color", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "By reminder time", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    @objc func defauleNoteSortOrder(_ sender: UITapGestureRecognizer){
        let alert = UIAlertController(title: "Default Note sort order", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "By modified time", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "By create time", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "By alphabet", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "By color", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "By reminder time", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    @objc func syncOnLaunch(_ sender: UITapGestureRecognizer){
        
    }
    @objc func listItemHeight(_ sender: UITapGestureRecognizer){
        let alert = UIAlertController(title: "Default item height", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Small", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Medium", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "High", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    @objc func defaultFontSize(_ sender: UITapGestureRecognizer){
        let alert = UIAlertController(title: "Default font size", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Small", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Medium", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "High", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    @objc func defaultColor(_ sender: UITapGestureRecognizer){
        let alert = UIAlertController(title: "Default color", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Red", style: .default, handler: { action in
            UserDefaults.standard.set(ColorDefine.colorRed, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorRed)
        }))
        alert.addAction(UIAlertAction(title: "Grey", style: .default, handler: { action in
            UserDefaults.standard.set(ColorDefine.colorGrey, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorGrey)
        }))
        alert.addAction(UIAlertAction(title: "Blue", style: .default, handler: { action in
            UserDefaults.standard.set(ColorDefine.colorBlue, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorBlue)
        }))
        alert.addAction(UIAlertAction(title: "Moss", style: .default, handler: { action in
            UserDefaults.standard.set(ColorDefine.colorMoss, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorMoss)
        }))
        alert.addAction(UIAlertAction(title: "White", style: .default, handler: { action in
            UserDefaults.standard.set(ColorDefine.colorWhite, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorWhite)
        }))
        alert.addAction(UIAlertAction(title: "Green", style: .default, handler: { action in
            UserDefaults.standard.set(ColorDefine.colorGreen, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorGreen)
        }))
        alert.addAction(UIAlertAction(title: "Black", style: .default, handler: { action in
            UserDefaults.standard.set(ColorDefine.colorBlack, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorBlack)
        }))
        alert.addAction(UIAlertAction(title: "Yellow", style: .default, handler: { action in
            UserDefaults.standard.set(ColorDefine.colorYellow, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorYellow)
        }))
        alert.addAction(UIAlertAction(title: "Purple", style: .default, handler: { action in
            UserDefaults.standard.set(ColorDefine.colorPurple, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorPurple)
        }))
        alert.addAction(UIAlertAction(title: "Orange", style: .default, handler: { action in
            UserDefaults.standard.set(ColorDefine.colorOrange, forKey: "colorDefault")
            self.viewDefaultColor.backgroundColor = UIColor(hexString: ColorDefine.colorOrange)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel")
        }))
        self.present(alert, animated: true)
    }
    @objc func defaultScreen(_ sender: UITapGestureRecognizer){
        let alert = UIAlertController(title: "Default Screen", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Textnote", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "CheckList", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    


}
