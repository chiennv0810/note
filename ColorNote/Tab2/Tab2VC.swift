//
//  Tab2VC.swift
//  ColorNote
//
//  Created by Nguyen van chien on 12/22/20.
//

import UIKit
import Toast_Swift

class Tab2VC: UIViewController {
    var listDataTextNote = [ModelTextNote]()
    var indexSelect = -1
    @IBOutlet weak var clvData: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataArchive(_:)), name: NSNotification.Name(rawValue: "reloadDataArchive"), object: nil)
        listDataTextNote = TextNoteEntity.shared.getArchive()
        clvData.dataSource = self
        clvData.delegate = self
        clvData.backgroundColor = .clear
        clvData.register(UINib(nibName: "CellList", bundle: nil), forCellWithReuseIdentifier: "CellList")

    }
    
    @objc func reloadDataArchive(_ sender: Notification){
        listDataTextNote = TextNoteEntity.shared.getArchive()
        clvData.reloadData()
    }
}

extension Tab2VC: UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listDataTextNote.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clvData.dequeueReusableCell(withReuseIdentifier: "CellList", for: indexPath) as! CellList
        cell.viewBackground.backgroundColor = UIColor(hexString: listDataTextNote[indexPath.row].color).withAlphaComponent(0.33)
        cell.viewColorLeft.backgroundColor = UIColor(hexString: listDataTextNote[indexPath.row].color)
        cell.lblTitle.text = listDataTextNote[indexPath.row].title
        if listDataTextNote[indexPath.row].modifyTime == "" {
            cell.lblDate.text = listDataTextNote[indexPath.row].createTime
        } else {
            cell.lblDate.text = listDataTextNote[indexPath.row].modifyTime
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: scale * 365, height: scale * 69)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: scale * 75, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indexSelect = indexPath.row

        if indexSelect == indexPath.row {
            let type = TextNoteEntity.shared.getType(idCheck: self.listDataTextNote[indexPath.row].id)
            var nameType = ""
            if type == 1 {
                nameType = "Text note"
            } else {
                nameType = "Checklist"
            }
            let alert = UIAlertController(title: "UnArchive " + nameType, message: "After unarchive, this " + nameType + " will return to the home page", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "UnArchive", style: .default, handler: { action in
                TextNoteEntity.shared.updateArchive(idCheck: self.listDataTextNote[indexPath.row].id, archiveUpdate: TextNoteEntity.shared.getStatusArchive(idCheck: self.listDataTextNote[indexPath.row].id))
                self.listDataTextNote = TextNoteEntity.shared.getArchive()
                self.clvData.reloadData()
                self.navigationController?.view.makeToast("This " + nameType + " unArchive")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadData"), object: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                print("Cancel")
            }))
            self.present(alert, animated: true)
        }
    }
    
}
